package controller;
import java.util.ArrayList;
import model.Consulta;


public class ControleConsulta {

public ArrayList<Consulta> listaConsulta;
	
	public void ControlaConsulta(){
		listaConsulta = new ArrayList<Consulta>();
	}
	
	public void adicionar(Consulta consulta){
		listaConsulta.add(consulta);
	
	}
	
	public void remover(Consulta consulta){
		listaConsulta.remove(consulta);
	
	}
	
	public Consulta pesquisarConsulta(String tipoConsulta){
		for(Consulta consulta : listaConsulta){
			if(consulta.getTipoConsulta().equalsIgnoreCase(tipoConsulta)){
				return consulta;
			}
		}
		return null;
	}
	
	
}