package model;

	public class Consulta{

		private String tipoConsulta;
		private String dataConsulta;
		private Paciente paciente;
		private Medico medico;
		
		public String getTipoConsulta() {
			return tipoConsulta;
		}
		
		public void setTipoConsulta(String tipoConsulta){
			this.tipoConsulta = tipoConsulta;
		}
		
		public String getDataConsulta() {
			return dataConsulta;
		}
		
		public void setDataConsulta(String dataConsulta){
			this.dataConsulta = dataConsulta;
		}
		
		
		public Paciente getPaciente() {
			return paciente;
		}
		public void setPaciente(Paciente paciente) {
			this.paciente = paciente;
		}
		
		public Medico getMedico() {
			return medico;
		}
		public void setMedico(Medico medico) {
			this.medico = medico;
		}
		
	}
